# StrawberryFields Gin

- [3.0.0]
- [2.9.0] - 2024-08-10 - 7 files changed, 191 insertions(+), 104 deletions(-)
- [2.8.0] - 2024-03-10 - 2 files changed, 162 insertions(+), 47 deletions(-)
- [2.7.0] - 2024-02-04 - 5 files changed, 121 insertions(+), 87 deletions(-)
- [2.6.0] - 2023-11-11 - 9 files changed, 35 insertions(+), 24 deletions(-)
- [2.5.0] - 2023-11-05 - 11 files changed, 326 insertions(+), 213 deletions(-)
- [2.4.0] - 2023-04-16 - 6 files changed, 174 insertions(+), 119 deletions(-)
- [2.3.0] - 2022-11-27 - 7 files changed, 147 insertions(+), 30 deletions(-)
- [2.2.0] - 2022-11-20 - 3 files changed, 23 insertions(+), 23 deletions(-)
- [2.1.0] - 2022-06-26 - 3 files changed, 20 insertions(+), 31 deletions(-)
- [2.0.0] - 2022-03-13 - 4 files changed, 305 insertions(+), 29 deletions(-)
- [1.3.0] - 2021-10-11 - 3 files changed, 102 insertions(+), 11 deletions(-)
- [1.2.0] - 2021-10-01 - 3 files changed, 13 insertions(+), 3 deletions(-)
- [1.1.0] - 2021-09-25 - 4 files changed, 61 insertions(+), 47 deletions(-)
- 1.0.0 - 2021-04-29

[3.0.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/fc881b96...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/89c7ecce...fc881b96
[2.8.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/7961ab23...89c7ecce
[2.7.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/dca081f6...7961ab23
[2.6.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/91046061...dca081f6
[2.5.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/e38eeb3a...91046061
[2.4.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/0ab4f13d...e38eeb3a
[2.3.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/5e1e10ca...0ab4f13d
[2.2.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/884416f8...5e1e10ca
[2.1.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/320872a7...884416f8
[2.0.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/21c2ccd9...320872a7
[1.3.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/ec278347...21c2ccd9
[1.2.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/e09bec4d...ec278347
[1.1.0]: https://gitlab.com/acefed/strawberryfields-gin/-/compare/700f18e2...e09bec4d
