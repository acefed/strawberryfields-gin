package main

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/joho/godotenv"
)

func getOrDefault[T any](m map[string]any, k string, v T) T {
	if x, exists := m[k]; exists {
		if y, ok := x.(T); ok {
			return y
		}
	}
	return v
}

func importPrivateKey(x string) *rsa.PrivateKey {
	x = strings.Join(strings.Split(x, "\\n"), "\n")
	x = strings.TrimPrefix(x, "\"")
	x = strings.TrimSuffix(x, "\"")
	y, _ := pem.Decode([]byte(x))
	if y == nil || y.Type != "PRIVATE KEY" {
		log.Fatal("error: importPrivateKey")
	}
	z, err := x509.ParsePKCS8PrivateKey(y.Bytes)
	if err != nil {
		log.Fatal(err)
	}
	return z.(*rsa.PrivateKey)
}

func exportPublicKey(x *rsa.PublicKey) string {
	y, err := x509.MarshalPKIXPublicKey(x)
	if err != nil {
		log.Fatal(err)
	}
	z := pem.EncodeToMemory(
		&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: y,
		},
	)
	return string(z)
}

func uuidv7() string {
	var v [16]byte
	if _, err := rand.Read(v[:]); err != nil {
		log.Fatal(err)
	}
	ts := uint64(time.Now().UnixMilli())
	v[0] = byte(ts >> 40)
	v[1] = byte(ts >> 32)
	v[2] = byte(ts >> 24)
	v[3] = byte(ts >> 16)
	v[4] = byte(ts >> 8)
	v[5] = byte(ts)
	v[6] = (v[6] & 0x0F) | 0x70
	v[8] = (v[8] & 0x3F) | 0x80
	return hex.EncodeToString(v[:])
}

func talkScript(req string) string {
	u, err := url.Parse(req)
	if err != nil {
		log.Fatal(err)
	}
	ts := strconv.FormatInt(time.Now().UnixMilli(), 10)
	if u.Hostname() == "localhost" {
		return "<p>" + ts + "</p>"
	}
	return strings.Join(
		[]string{
			"<p>",
			"<a href=\"https://",
			u.Hostname(),
			"/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">",
			u.Hostname(),
			"</a>",
			"</p>",
		},
		"",
	)
}

func getActivity(username, hostname, req string) []byte {
	privateKey := importPrivateKey(os.Getenv("PRIVATE_KEY"))
	u, err := url.Parse(req)
	if err != nil {
		log.Fatal(err)
	}
	t := time.Now().Format("Mon, 02 Jan 2006 15:04:05 GMT")
	x := sha256.Sum256(
		[]byte(strings.Join(
			[]string{
				"(request-target): get " + u.Path,
				"host: " + u.Hostname(),
				"date: " + t,
			},
			"\n",
		)),
	)
	sig, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, x[:])
	if err != nil {
		log.Fatal(err)
	}
	b64 := base64.StdEncoding.EncodeToString(sig[:])
	headers := map[string]string{
		"Date": t,
		"Signature": strings.Join(
			[]string{
				"keyId=\"https://" + hostname + "/u/" + username + "#Key\"",
				"algorithm=\"rsa-sha256\"",
				"headers=\"(request-target) host date\"",
				"signature=\"" + b64 + "\"",
			},
			",",
		),
		"Accept":          "application/activity+json",
		"Accept-Encoding": "identity",
		"Cache-Control":   "no-cache",
		"User-Agent":      "StrawberryFields-Gin/3.0.0 (+https://" + hostname + "/)",
	}
	client := resty.New()
	res, err := client.R().SetHeaders(headers).Get(req)
	if err != nil {
		log.Fatal(err)
	}
	status := strconv.Itoa(res.StatusCode())
	fmt.Println("GET " + req + " " + status)
	return res.Body()
}

func postActivity(username, hostname, req string, x map[string]any) {
	var body bytes.Buffer
	enc := json.NewEncoder(&body)
	enc.SetEscapeHTML(false)
	if err := enc.Encode(x); err != nil {
		log.Fatal(err)
	}
	privateKey := importPrivateKey(os.Getenv("PRIVATE_KEY"))
	u, err := url.Parse(req)
	if err != nil {
		log.Fatal(err)
	}
	t := time.Now().Format("Mon, 02 Jan 2006 15:04:05 GMT")
	y := sha256.Sum256(body.Bytes())
	s256 := base64.StdEncoding.EncodeToString(y[:])
	z := sha256.Sum256(
		[]byte(strings.Join(
			[]string{
				"(request-target): post " + u.Path,
				"host: " + u.Hostname(),
				"date: " + t,
				"digest: SHA-256=" + s256,
			},
			"\n",
		)),
	)
	sig, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, z[:])
	if err != nil {
		log.Fatal(err)
	}
	b64 := base64.StdEncoding.EncodeToString(sig[:])
	headers := map[string]string{
		"Date":   t,
		"Digest": "SHA-256=" + s256,
		"Signature": strings.Join(
			[]string{
				"keyId=\"https://" + hostname + "/u/" + username + "#Key\"",
				"algorithm=\"rsa-sha256\"",
				"headers=\"(request-target) host date digest\"",
				"signature=\"" + b64 + "\"",
			},
			",",
		),
		"Accept":          "application/json",
		"Accept-Encoding": "gzip",
		"Cache-Control":   "max-age=0",
		"Content-Type":    "application/activity+json",
		"User-Agent":      "StrawberryFields-Gin/3.0.0 (+https://" + hostname + "/)",
	}
	fmt.Println("POST " + req + " " + body.String())
	client := resty.New()
	if _, err := client.R().SetHeaders(headers).SetBody(body.String()).Post(req); err != nil {
		log.Fatal(err)
	}
}

func acceptFollow(username, hostname string, x, y map[string]any) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid,
		"type":     "Accept",
		"actor":    "https://" + hostname + "/u/" + username,
		"object":   y,
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func follow(username, hostname string, x map[string]any) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid,
		"type":     "Follow",
		"actor":    "https://" + hostname + "/u/" + username,
		"object":   x["id"],
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func undoFollow(username, hostname string, x map[string]any) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo",
		"type":     "Undo",
		"actor":    "https://" + hostname + "/u/" + username,
		"object": map[string]any{
			"id":     "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":   "Follow",
			"actor":  "https://" + hostname + "/u/" + username,
			"object": x["id"],
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func like(username, hostname string, x, y map[string]any) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid,
		"type":     "Like",
		"actor":    "https://" + hostname + "/u/" + username,
		"object":   x["id"],
	}
	postActivity(username, hostname, y["inbox"].(string), body)
}

func undoLike(username, hostname string, x, y map[string]any) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo",
		"type":     "Undo",
		"actor":    "https://" + hostname + "/u/" + username,
		"object": map[string]any{
			"id":     "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":   "Like",
			"actor":  "https://" + hostname + "/u/" + username,
			"object": x["id"],
		},
	}
	postActivity(username, hostname, y["inbox"].(string), body)
}

func announce(username, hostname string, x, y map[string]any) {
	aid := uuidv7()
	t := time.Now().Format(time.RFC3339)
	body := map[string]any{
		"@context":  "https://www.w3.org/ns/activitystreams",
		"id":        "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity",
		"type":      "Announce",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object":    x["id"],
	}
	postActivity(username, hostname, y["inbox"].(string), body)
}

func undoAnnounce(username, hostname string, x, y map[string]any, z string) {
	aid := uuidv7()
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo",
		"type":     "Undo",
		"actor":    "https://" + hostname + "/u/" + username,
		"object": map[string]any{
			"id":     z + "/activity",
			"type":   "Announce",
			"actor":  "https://" + hostname + "/u/" + username,
			"to":     []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":     []string{"https://" + hostname + "/u/" + username + "/followers"},
			"object": x["id"],
		},
	}
	postActivity(username, hostname, y["inbox"].(string), body)
}

func createNote(username, hostname string, x map[string]any, y string) {
	aid := uuidv7()
	t := time.Now().Format(time.RFC3339)
	body := map[string]any{
		"@context":  "https://www.w3.org/ns/activitystreams",
		"id":        "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity",
		"type":      "Create",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object": map[string]any{
			"id":           "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":         "Note",
			"attributedTo": "https://" + hostname + "/u/" + username,
			"content":      talkScript(y),
			"url":          "https://" + hostname + "/u/" + username + "/s/" + aid,
			"published":    t,
			"to":           []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":           []string{"https://" + hostname + "/u/" + username + "/followers"},
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func createNoteImage(username, hostname string, x map[string]any, y, z string) {
	aid := uuidv7()
	t := time.Now().Format(time.RFC3339)
	body := map[string]any{
		"@context":  "https://www.w3.org/ns/activitystreams",
		"id":        "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity",
		"type":      "Create",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object": map[string]any{
			"id":           "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":         "Note",
			"attributedTo": "https://" + hostname + "/u/" + username,
			"content":      talkScript("https://localhost"),
			"url":          "https://" + hostname + "/u/" + username + "/s/" + aid,
			"published":    t,
			"to":           []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":           []string{"https://" + hostname + "/u/" + username + "/followers"},
			"attachment": []any{
				map[string]string{
					"type":      "Image",
					"mediaType": z,
					"url":       y,
				},
			},
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func createNoteMention(username, hostname string, x, y map[string]any, z string) {
	aid := uuidv7()
	t := time.Now().Format(time.RFC3339)
	u, err := url.Parse(y["inbox"].(string))
	if err != nil {
		log.Fatal(err)
	}
	at := "@" + y["preferredUsername"].(string) + "@" + u.Hostname()
	body := map[string]any{
		"@context":  "https://www.w3.org/ns/activitystreams",
		"id":        "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity",
		"type":      "Create",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object": map[string]any{
			"id":           "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":         "Note",
			"attributedTo": "https://" + hostname + "/u/" + username,
			"inReplyTo":    x["id"],
			"content":      talkScript(z),
			"url":          "https://" + hostname + "/u/" + username + "/s/" + aid,
			"published":    t,
			"to":           []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":           []string{"https://" + hostname + "/u/" + username + "/followers"},
			"tag": []any{
				map[string]string{
					"type": "Mention",
					"name": at,
				},
			},
		},
	}
	postActivity(username, hostname, y["inbox"].(string), body)
}

func createNoteHashtag(username, hostname string, x map[string]any, y, z string) {
	aid := uuidv7()
	t := time.Now().Format(time.RFC3339)
	body := map[string]any{
		"@context": []any{
			"https://www.w3.org/ns/activitystreams",
			map[string]string{"Hashtag": "as:Hashtag"},
		},
		"id":        "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity",
		"type":      "Create",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object": map[string]any{
			"id":           "https://" + hostname + "/u/" + username + "/s/" + aid,
			"type":         "Note",
			"attributedTo": "https://" + hostname + "/u/" + username,
			"content":      talkScript(y),
			"url":          "https://" + hostname + "/u/" + username + "/s/" + aid,
			"published":    t,
			"to":           []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":           []string{"https://" + hostname + "/u/" + username + "/followers"},
			"tag": []any{
				map[string]string{
					"type": "Hashtag",
					"name": "#" + z,
				},
			},
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func updateNote(username, hostname string, x map[string]any, y string) {
	t := time.Now().Format(time.RFC3339)
	ts := strconv.FormatInt(time.Now().UnixMilli(), 10)
	body := map[string]any{
		"@context":  "https://www.w3.org/ns/activitystreams",
		"id":        y + "#" + ts,
		"type":      "Update",
		"actor":     "https://" + hostname + "/u/" + username,
		"published": t,
		"to":        []string{"https://www.w3.org/ns/activitystreams#Public"},
		"cc":        []string{"https://" + hostname + "/u/" + username + "/followers"},
		"object": map[string]any{
			"id":           y,
			"type":         "Note",
			"attributedTo": "https://" + hostname + "/u/" + username,
			"content":      talkScript("https://localhost"),
			"url":          y,
			"updated":      t,
			"to":           []string{"https://www.w3.org/ns/activitystreams#Public"},
			"cc":           []string{"https://" + hostname + "/u/" + username + "/followers"},
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func deleteTombstone(username, hostname string, x map[string]any, y string) {
	body := map[string]any{
		"@context": "https://www.w3.org/ns/activitystreams",
		"id":       y + "#Delete",
		"type":     "Delete",
		"actor":    "https://" + hostname + "/u/" + username,
		"object": map[string]any{
			"id":   y,
			"type": "Tombstone",
		},
	}
	postActivity(username, hostname, x["inbox"].(string), body)
}

func main() {
	godotenv.Load()
	port := "8080"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	var configJSON []byte
	if os.Getenv("CONFIG_JSON") != "" {
		configJSON = []byte(os.Getenv("CONFIG_JSON"))
		configJSON = bytes.TrimPrefix(configJSON, []byte("'"))
		configJSON = bytes.TrimSuffix(configJSON, []byte("'"))
	} else {
		var err error
		configJSON, err = os.ReadFile("data/config.json")
		if err != nil {
			log.Fatal(err)
		}
	}
	var config map[string]any
	if err := json.Unmarshal(configJSON, &config); err != nil {
		log.Fatal(err)
	}

	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("static"))))

	r.GET("/", func(c *gin.Context) {
		c.String(200, "StrawberryFields Gin")
	})

	r.GET("/about", func(c *gin.Context) {
		c.String(200, "About: Blank")
	})

	r.GET("/u/:username", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)

		mine, err := url.Parse(obj["me"].(string))
		if err != nil {
			log.Fatal(err)
		}
		me := strings.Join(
			[]string{
				"<a href=\"https://",
				mine.Hostname(),
				"/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">",
				"https://",
				mine.Hostname(),
				"/",
				"</a>",
			},
			"",
		)
		privateKey := importPrivateKey(os.Getenv("PRIVATE_KEY"))
		publicKeyPEM := exportPublicKey(&privateKey.PublicKey)

		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		ttl := strconv.FormatFloat(config["ttl"].(float64), 'f', 0, 64)
		username := c.Param("username")
		hostname := u.Hostname()
		acceptHeaderField := c.GetHeader("Accept")
		hasType := false
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		if strings.Contains(acceptHeaderField, "application/activity+json") {
			hasType = true
		}
		if strings.Contains(acceptHeaderField, "application/ld+json") {
			hasType = true
		}
		if strings.Contains(acceptHeaderField, "application/json") {
			hasType = true
		}
		if !hasType {
			body := username + ": " + obj["name"].(string)
			headers := map[string]string{
				"Cache-Control": "public, max-age=" + ttl + ", must-revalidate",
				"Vary":          "Accept, Accept-Encoding",
			}
			for k, v := range headers {
				c.Header(k, v)
			}
			c.String(200, body)
			return
		}
		body := gin.H{
			"@context": []any{
				"https://www.w3.org/ns/activitystreams",
				"https://w3id.org/security/v1",
				map[string]string{
					"schema":        "https://schema.org/",
					"PropertyValue": "schema:PropertyValue",
					"value":         "schema:value",
					"Key":           "sec:Key",
				},
			},
			"id":                "https://" + hostname + "/u/" + username,
			"type":              "Person",
			"inbox":             "https://" + hostname + "/u/" + username + "/inbox",
			"outbox":            "https://" + hostname + "/u/" + username + "/outbox",
			"following":         "https://" + hostname + "/u/" + username + "/following",
			"followers":         "https://" + hostname + "/u/" + username + "/followers",
			"preferredUsername": username,
			"name":              obj["name"].(string),
			"summary":           "<p>3.0.0</p>",
			"url":               "https://" + hostname + "/u/" + username,
			"endpoints":         map[string]string{"sharedInbox": "https://" + hostname + "/u/" + username + "/inbox"},
			"attachment": []any{
				map[string]string{
					"type":  "PropertyValue",
					"name":  "me",
					"value": me,
				},
			},
			"icon": map[string]string{
				"type":      "Image",
				"mediaType": "image/png",
				"url":       "https://" + hostname + "/static/" + username + "u.png",
			},
			"image": map[string]string{
				"type":      "Image",
				"mediaType": "image/png",
				"url":       "https://" + hostname + "/static/" + username + "s.png",
			},
			"publicKey": map[string]string{
				"id":           "https://" + hostname + "/u/" + username + "#Key",
				"type":         "Key",
				"owner":        "https://" + hostname + "/u/" + username,
				"publicKeyPem": publicKeyPEM,
			},
		}
		headers := map[string]string{
			"Cache-Control": "public, max-age=" + ttl + ", must-revalidate",
			"Vary":          "Accept, Accept-Encoding",
			"Content-Type":  "application/activity+json",
		}
		for k, v := range headers {
			c.Header(k, v)
		}
		c.PureJSON(200, body)
	})

	r.GET("/u/:username/inbox", func(c *gin.Context) {
		c.Status(405)
	})

	r.POST("/u/:username/inbox", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		username := c.Param("username")
		hostname := u.Hostname()
		contentTypeHeaderField := c.GetHeader("Content-Type")
		hasType := false
		var y map[string]any
		if err := c.ShouldBindJSON(&y); err != nil {
			c.Status(400)
			return
		}
		t := getOrDefault(y, "type", "")
		aid := getOrDefault(y, "id", "")
		atype := getOrDefault(y, "type", "")
		if len(aid) > 1024 || len(atype) > 64 {
			c.Status(400)
			return
		}
		fmt.Println("INBOX " + aid + " " + atype)
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		if strings.Contains(contentTypeHeaderField, "application/activity+json") {
			hasType = true
		}
		if strings.Contains(contentTypeHeaderField, "application/ld+json") {
			hasType = true
		}
		if strings.Contains(contentTypeHeaderField, "application/json") {
			hasType = true
		}
		if !hasType {
			c.Status(400)
			return
		}
		if c.GetHeader("Digest") == "" || c.GetHeader("Signature") == "" {
			c.Status(400)
			return
		}
		if t == "Accept" || t == "Reject" || t == "Add" {
			c.Status(200)
			return
		}
		if t == "Remove" || t == "Like" || t == "Announce" {
			c.Status(200)
			return
		}
		if t == "Create" || t == "Update" || t == "Delete" {
			c.Status(200)
			return
		}
		if t == "Follow" {
			if u, err := url.Parse(getOrDefault(y, "actor", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var x map[string]any
			if err := json.Unmarshal(getActivity(username, hostname, y["actor"].(string)), &x); err != nil {
				c.Status(500)
				return
			}
			if len(x) == 0 {
				c.Status(500)
				return
			}
			acceptFollow(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "Undo" {
			z := getOrDefault(y, "object", map[string]any{})
			t = getOrDefault(z, "type", "")
			if t == "Accept" || t == "Like" || t == "Announce" {
				c.Status(200)
				return
			}
			if t == "Follow" {
				if u, err := url.Parse(getOrDefault(y, "actor", "")); u.Scheme != "https" || err != nil {
					c.Status(400)
					return
				}
				var x map[string]any
				if err := json.Unmarshal(getActivity(username, hostname, y["actor"].(string)), &x); err != nil {
					c.Status(500)
					return
				}
				if len(x) == 0 {
					c.Status(500)
					return
				}
				acceptFollow(username, hostname, x, z)
				c.Status(200)
				return
			}
		}
		c.Status(500)
	})

	r.POST("/u/:username/outbox", func(c *gin.Context) {
		c.Status(405)
	})

	r.GET("/u/:username/outbox", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		username := c.Param("username")
		hostname := u.Hostname()
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		body := gin.H{
			"@context":   "https://www.w3.org/ns/activitystreams",
			"id":         "https://" + hostname + "/u/" + username + "/outbox",
			"type":       "OrderedCollection",
			"totalItems": 0,
		}
		c.Header("Content-Type", "application/activity+json")
		c.JSON(200, body)
	})

	r.GET("/u/:username/following", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		username := c.Param("username")
		hostname := u.Hostname()
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		body := gin.H{
			"@context":   "https://www.w3.org/ns/activitystreams",
			"id":         "https://" + hostname + "/u/" + username + "/following",
			"type":       "OrderedCollection",
			"totalItems": 0,
		}
		c.Header("Content-Type", "application/activity+json")
		c.JSON(200, body)
	})

	r.GET("/u/:username/followers", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		username := c.Param("username")
		hostname := u.Hostname()
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		body := gin.H{
			"@context":   "https://www.w3.org/ns/activitystreams",
			"id":         "https://" + hostname + "/u/" + username + "/followers",
			"type":       "OrderedCollection",
			"totalItems": 0,
		}
		c.Header("Content-Type", "application/activity+json")
		c.JSON(200, body)
	})

	r.POST("/s/:secret/u/:username", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		username := c.Param("username")
		hostname := u.Hostname()
		var send map[string]any
		if err := c.ShouldBindJSON(&send); err != nil {
			c.Status(400)
			return
		}
		t := getOrDefault(send, "type", "")
		if username != obj["preferredUsername"].(string) {
			c.String(404, "404 page not found")
			return
		}
		if c.Param("secret") == "" || c.Param("secret") == "-" {
			c.String(404, "404 page not found")
			return
		}
		if c.Param("secret") != os.Getenv("SECRET") {
			c.String(404, "404 page not found")
			return
		}
		if u, err := url.Parse(getOrDefault(send, "id", "")); u.Scheme != "https" || err != nil {
			c.Status(400)
			return
		}
		var x map[string]any
		if err := json.Unmarshal(getActivity(username, hostname, send["id"].(string)), &x); err != nil {
			c.Status(500)
			return
		}
		if len(x) == 0 {
			c.Status(500)
			return
		}
		aid := getOrDefault(x, "id", "")
		atype := getOrDefault(x, "type", "")
		if len(aid) > 1024 || len(atype) > 64 {
			c.Status(400)
			return
		}
		if t == "follow" {
			follow(username, hostname, x)
			c.Status(200)
			return
		}
		if t == "undo_follow" {
			undoFollow(username, hostname, x)
			c.Status(200)
			return
		}
		if t == "like" {
			if u, err := url.Parse(getOrDefault(x, "attributedTo", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var y map[string]any
			if err := json.Unmarshal(
				getActivity(username, hostname, x["attributedTo"].(string)), &y,
			); err != nil {
				c.Status(500)
				return
			}
			if len(y) == 0 {
				c.Status(500)
				return
			}
			like(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "undo_like" {
			if u, err := url.Parse(getOrDefault(x, "attributedTo", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var y map[string]any
			if err := json.Unmarshal(
				getActivity(username, hostname, x["attributedTo"].(string)), &y,
			); err != nil {
				c.Status(500)
				return
			}
			if len(y) == 0 {
				c.Status(500)
				return
			}
			undoLike(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "announce" {
			if u, err := url.Parse(getOrDefault(x, "attributedTo", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var y map[string]any
			if err := json.Unmarshal(
				getActivity(username, hostname, x["attributedTo"].(string)), &y,
			); err != nil {
				c.Status(500)
				return
			}
			if len(y) == 0 {
				c.Status(500)
				return
			}
			announce(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "undo_announce" {
			if u, err := url.Parse(getOrDefault(x, "attributedTo", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var y map[string]any
			if err := json.Unmarshal(
				getActivity(username, hostname, x["attributedTo"].(string)), &y,
			); err != nil {
				c.Status(500)
				return
			}
			if len(y) == 0 {
				c.Status(500)
				return
			}
			var z string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				z = v.(string)
			} else {
				z = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000"
			}
			if u, err := url.Parse(z); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			undoAnnounce(username, hostname, x, y, z)
			c.Status(200)
			return
		}
		if t == "create_note" {
			var y string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				y = v.(string)
			} else {
				y = "https://localhost"
			}
			if u, err := url.Parse(y); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			createNote(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "create_note_image" {
			var y string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				y = v.(string)
			} else {
				y = "https://" + hostname + "/static/logo.png"
			}
			if u, err := url.Parse(y); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			if u, err := url.Parse(y); u.Hostname() != hostname || err != nil {
				c.Status(400)
				return
			}
			z := "image/png"
			if strings.HasSuffix(y, ".jpg") || strings.HasSuffix(y, ".jpeg") {
				z = "image/jpeg"
			}
			if strings.HasSuffix(y, ".svg") {
				z = "image/svg+xml"
			}
			if strings.HasSuffix(y, ".gif") {
				z = "image/gif"
			}
			if strings.HasSuffix(y, ".webp") {
				z = "image/webp"
			}
			if strings.HasSuffix(y, ".avif") {
				z = "image/avif"
			}
			createNoteImage(username, hostname, x, y, z)
			c.Status(200)
			return
		}
		if t == "create_note_mention" {
			if u, err := url.Parse(getOrDefault(x, "attributedTo", "")); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			var y map[string]any
			if err := json.Unmarshal(
				getActivity(username, hostname, x["attributedTo"].(string)), &y,
			); err != nil {
				c.Status(500)
				return
			}
			if len(y) == 0 {
				c.Status(500)
				return
			}
			var z string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				z = v.(string)
			} else {
				z = "https://localhost"
			}
			if u, err := url.Parse(z); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			createNoteMention(username, hostname, x, y, z)
			c.Status(200)
			return
		}
		if t == "create_note_hashtag" {
			var y, z string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				y = v.(string)
			} else {
				y = "https://localhost"
			}
			if u, err := url.Parse(y); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			if v, ok := send["tag"]; ok && v != nil && v.(string) != "" {
				z = v.(string)
			} else {
				z = "Hashtag"
			}
			createNoteHashtag(username, hostname, x, y, z)
			c.Status(200)
			return
		}
		if t == "update_note" {
			var y string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				y = v.(string)
			} else {
				y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000"
			}
			if u, err := url.Parse(y); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			updateNote(username, hostname, x, y)
			c.Status(200)
			return
		}
		if t == "delete_tombstone" {
			var y string
			if v, ok := send["url"]; ok && v != nil && v.(string) != "" {
				y = v.(string)
			} else {
				y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000"
			}
			if u, err := url.Parse(y); u.Scheme != "https" || err != nil {
				c.Status(400)
				return
			}
			deleteTombstone(username, hostname, x, y)
			c.Status(200)
			return
		}
		fmt.Println("TYPE " + aid + " " + atype)
		c.Status(200)
	})

	r.GET("/.well-known/nodeinfo", func(c *gin.Context) {
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		ttl := strconv.FormatFloat(config["ttl"].(float64), 'f', 0, 64)
		hostname := u.Hostname()
		body := gin.H{
			"links": []any{
				map[string]string{
					"rel":  "http://nodeinfo.diaspora.software/ns/schema/2.0",
					"href": "https://" + hostname + "/nodeinfo/2.0.json",
				},
				map[string]string{
					"rel":  "http://nodeinfo.diaspora.software/ns/schema/2.1",
					"href": "https://" + hostname + "/nodeinfo/2.1.json",
				},
			},
		}
		headers := map[string]string{
			"Cache-Control": "public, max-age=" + ttl + ", must-revalidate",
			"Vary":          "Accept, Accept-Encoding",
		}
		for k, v := range headers {
			c.Header(k, v)
		}
		c.JSON(200, body)
	})

	r.GET("/.well-known/webfinger", func(c *gin.Context) {
		arr := config["actor"].([]any)
		obj := arr[0].(map[string]any)
		u, err := url.Parse(config["origin"].(string))
		if err != nil {
			log.Fatal(err)
		}
		ttl := strconv.FormatFloat(config["ttl"].(float64), 'f', 0, 64)
		username := obj["preferredUsername"].(string)
		hostname := u.Hostname()
		p443 := "https://" + hostname + ":443/"
		resource := c.Query("resource")
		hasResource := false
		if strings.HasPrefix(resource, p443) {
			resource = "https://" + hostname + "/" + strings.TrimPrefix(resource, p443)
		}
		if resource == "acct:"+username+"@"+hostname {
			hasResource = true
		}
		if resource == "mailto:"+username+"@"+hostname {
			hasResource = true
		}
		if resource == "https://"+hostname+"/@"+username {
			hasResource = true
		}
		if resource == "https://"+hostname+"/u/"+username {
			hasResource = true
		}
		if resource == "https://"+hostname+"/user/"+username {
			hasResource = true
		}
		if resource == "https://"+hostname+"/users/"+username {
			hasResource = true
		}
		if !hasResource {
			c.String(404, "404 page not found")
			return
		}
		body := gin.H{
			"subject": "acct:" + username + "@" + hostname,
			"aliases": []string{
				"mailto:" + username + "@" + hostname,
				"https://" + hostname + "/@" + username,
				"https://" + hostname + "/u/" + username,
				"https://" + hostname + "/user/" + username,
				"https://" + hostname + "/users/" + username,
			},
			"links": []any{
				map[string]string{
					"rel":  "self",
					"type": "application/activity+json",
					"href": "https://" + hostname + "/u/" + username,
				},
				map[string]string{
					"rel":  "http://webfinger.net/rel/avatar",
					"type": "image/png",
					"href": "https://" + hostname + "/static/" + username + "u.png",
				},
				map[string]string{
					"rel":  "http://webfinger.net/rel/profile-page",
					"type": "text/plain",
					"href": "https://" + hostname + "/u/" + username,
				},
			},
		}
		headers := map[string]string{
			"Cache-Control": "public, max-age=" + ttl + ", must-revalidate",
			"Vary":          "Accept, Accept-Encoding",
			"Content-Type":  "application/jrd+json",
		}
		for k, v := range headers {
			c.Header(k, v)
		}
		c.JSON(200, body)
	})

	r.GET("/@", func(c *gin.Context) {
		c.Redirect(302, "/")
	})

	r.GET("/u", func(c *gin.Context) {
		c.Redirect(302, "/")
	})

	r.GET("/user", func(c *gin.Context) {
		c.Redirect(302, "/")
	})

	r.GET("/users", func(c *gin.Context) {
		c.Redirect(302, "/")
	})

	r.GET("/users/:username", func(c *gin.Context) {
		c.Redirect(302, "/u/"+c.Param("username"))
	})

	r.GET("/user/:username", func(c *gin.Context) {
		c.Redirect(302, "/u/"+c.Param("username"))
	})

	r.GET("/@:username", func(c *gin.Context) {
		c.Redirect(302, "/u/"+c.Param("username"))
	})

	r.Run(":" + port)
}
